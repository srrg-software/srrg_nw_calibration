/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TYPEDEFS_H
#define TYPEDEFS_H

#include <Eigen/Dense>
#include <Eigen/Geometry>

namespace new_world_calibration{

  /** \typedef VectorX<T> is the template version of a VectorXf/Xd */
  template<typename T>
  using VectorX = Eigen::Matrix<T, Eigen::Dynamic, 1>;

  /** \typedef Vector3<T> is the template version of a Vector3f/3d */
  template<typename T>
  using Vector3 = Eigen::Matrix<T, 3, 1>;

  /** \typedef Vector2<T> is the template version of a Vector2f/2d */
  template<typename T>
  using Vector2 = Eigen::Matrix<T, 2, 1>;

  /** \typedef Vector6<T> is the template version of a Vector6f/6d */
  template<typename T>
  using Vector6 = Eigen::Matrix<T, 6, 1>;

  /** \typedef MatrixX<T> is the template version of a MatrixXf/Xd */
  template<typename T>
  using MatrixX = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;

  /** \typedef Matrix2<T> is the template version of a Matrix2f/2d */
  template<typename T>
  using Matrix2 = Eigen::Matrix<T, 2, 2>;

  /** \typedef Matrix3<T> is the template version of a Matrix3f/3d */
  template<typename T>
  using Matrix3 = Eigen::Matrix<T, 3, 3>;

  /** \typedef Matrix6<T> is the template version of a Matrix6f/6d */
  template<typename T>
  using Matrix6 = Eigen::Matrix<T, 6, 6>;

  /** \typedef Isometry2<T> is the template version of a Isometry2f/2d */
  template<typename T>
  using Isometry2 = Eigen::Transform<T, 2, Eigen::Isometry>;

  /** \typedef Isometry3<T> is the template version of a Isometry3f/3d */
  template<typename T>
  using Isometry3 = Eigen::Transform<T, 3, Eigen::Isometry>;

  /** \typedef AngleAxis<T> is the template version of a AngleAxisf/d */
  template<typename T>
  using AngleAxis = Eigen::AngleAxis<T>;

  /** \typedef Quaternion<T> is the template version of a Quaternionf/d */
  template<typename T>
  using Quaternion = Eigen::Quaternion<T>;

}

#endif


