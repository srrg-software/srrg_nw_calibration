/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _ODOM_PARAMS_HPP_
#define _ODOM_PARAMS_HPP_

#include "base_params.hpp"

namespace new_world_calibration{

  /**
   * OdomParams class.
   * @see BaseParams()
   * Extends BaseParams by adding a VectorX describing the
   * odometry parameters, i.e. wheel ratios and baseline(s)
   */
  template<typename T>
  class OdomParams:public BaseParams{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    /**
     * @brief a constructor
     * @param odom_params_, VectorX<T> containing the odometry parameters.
     * The order must satisty the description of parameter made in the platform kinematics
     * @param frame_, string defining the parameters frame
     * @param reference_frame_, string defining the parameters reference frame
     */
    OdomParams(const VectorX<T>& odom_params_,
               const std::string& frame_,
               const std::string& reference_frame_): BaseParams(frame_, reference_frame_), _odom_params(odom_params_)
    {_size = _odom_params.rows();}

    /**
     * @brief a copy constructor
     * @param CopyClass, a pointer to an OdomParams object
     */
    OdomParams(const OdomParams<T>* CopyClass) : BaseParams(CopyClass->frame(), CopyClass->reference_frame()),
      _odom_params(CopyClass->odomParams()), _size(CopyClass->size()){}

    /**
     * @brief a destructor
     */
    ~OdomParams(){}

    /**
     * @brief setter for _odom_params
     * @param odom_params_, VectorX<T>
     */
    void setOdomParams(const VectorX<T>& odom_params_){_odom_params = odom_params_;
                                                       _size = _odom_params.rows();}

    /**
     * @brief getter for _odom_params
     * @return a VetorX<T> containing the odometry parameters
     */
    const VectorX<T> odomParams() const {return _odom_params;}

    /**
     * @brief getter for size of _odom_params
     * @return integer describing the size of odometry parameter set
     */
    const int size() const {return _size;}

    /**
     * @brief this method updates the current value of the odometry parameters
     * The update is done by simply adding the provided increment to the
     * current value of _odom_params
     * @param increment, a VectorX<T> of the same size of _odom_params
     */
    void updateParams(const VectorX<T>& increment){ _odom_params += increment;}

    /**
     * @brief this method prints the current parameter infoes
     */
    void printStatus(){
      std::cerr<<"P ["<<this->_reference_frame<<" -> "<<this->_frame<<"]: "<< _odom_params.transpose()<<std::endl;}

  private:
    VectorX<T> _odom_params; /** \var _odom_params stores the odometry parameters */
    int _size; /** \var _size stores the size of _odom_params */
  };

}

#endif
