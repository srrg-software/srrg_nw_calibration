/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _BASE_PARAMS_HPP
#define _BASE_PARAMS_HPP

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <stdlib.h>
#include <vector>
#include <iostream>

#include "utils.hpp"
#include "typedefs.h"

namespace new_world_calibration{

  /**
   * BaseParams class.
   * describe how is composed a params set to be calibrated.
   * It will be extended in odom and sensor params.
   * Each parameter set is composed by two strings, used to
   * describe the reference and current frame,
   * e.g. base_link -> camera_left
   * and the corresponding parameter
   * e.g. relative transform, or odometry parameters
   */
  class BaseParams{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    /**
     * @brief a constructor
     * @param frame_, string defining the current frame
     * @param reference_frame_, string defining the reference frame
     */
    BaseParams(const std::string& frame_, const std::string& reference_frame_) : _frame(frame_), _reference_frame(reference_frame_){}

    /**
     * @brief a destructor
     */
    ~BaseParams(){}

    /**
     * @brief setter for the frame
     * @param frame_, a string
     */
    void setFrame(const std::string& frame_){_frame = frame_;}
    /**
     * @brief setter for the reference frame
     * @param reference_frame_, a string
     */
    void setReferenceFrame(const std::string& reference_frame_){_reference_frame = reference_frame_;}

    /**
     * @brief getter for the frame
     * @return a string corresponding to the frame
     */
    const std::string frame() const {return _frame;}
    /**
     * @brief getter for the reference_frame
     * @return a string corresponding to the reference_frame
     */
    const std::string reference_frame() const {return _reference_frame;}

    /**
     * @brief this methods switch reference and current frame
     * e.g. can be used after an inversion to keep track
     * of the frames
     */
    void switchFrames(){std::string old_frame;
                        old_frame = frame();
                        _frame = _reference_frame;
                        _reference_frame = old_frame;}

  protected:
    std::string _frame; /** \var _frame of the parameter set */
    std::string _reference_frame; /** \var _reference_frame of the parameter set */
  };

}

#endif
