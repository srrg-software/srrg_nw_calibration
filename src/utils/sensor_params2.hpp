/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _SENSOR_PARAMS2_HPP_
#define _SENSOR_PARAMS2_HPP_

#include "base_params.hpp"

namespace new_world_calibration{

  /**
   * SensorParams2 class.
   * @see BaseParams()
   * Extends BaseParams by adding a 2d Isometry describing the
   * 2d pose of a sensor w.r.t the platform
   */
  template<typename T>
  class SensorParams2:public BaseParams{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    /**
     * @brief a constructor
     * @param isometry_, Isometry2<T> describing the pose of a 2d sensor wrt _reference_frame
     * @param frame_, string declaring the sensor frame
     * @param reference_frame_, string declaring the reference_frame
     */
    SensorParams2(const Isometry2<T>& isometry_,
                  const std::string& frame_,
                  const std::string& reference_frame_) : BaseParams(frame_, reference_frame_), _isometry(isometry_){}
    /**
     * @brief a copy constructor
     * @param CopyClass, a pointer to SensorParams2<T>
     */
    SensorParams2(SensorParams2<T>* CopyClass) : BaseParams(CopyClass->frame(), CopyClass->reference_frame()), _isometry(CopyClass->isometry()){}

    /**
     * @brief a destructor
     */
    ~SensorParams2(){}

    /**
     * @brief setter of _isometry
     * @param isometry_, Isometry2<T>
     */
    void setIsometry(const Isometry2<T>& isometry_) { this->_isometry = isometry_;}

    /**
     * @brief getter of _isometry
     * @return a Isometry2<T> containing the 2d sensor pose wrt _reference_frame
     */
    Isometry2<T> isometry(){return _isometry;}

    /**
     * @brief this method updates the current value of the sensor parameters
     * The update is performed at manifold level
     * @param increment, an Isometry2<T>
     */
    void updateParams(const Isometry2<T>& increment){
      _isometry = _isometry * increment;
      Matrix2<T> R = _isometry.linear();
      Matrix2<T> E = R.transpose() * R;
      E.diagonal().array() -= 1;
      _isometry.linear() -= 0.5 * R * E;
    }

    /**
     * @brief this method prints the current parameter infoes
     */
    void printStatus(){
      std::cerr<<"P ["<<this->_reference_frame<<" -> "<<this->_frame<<"]: "<< t2v(_isometry).transpose()<<std::endl;}

  private:
    Isometry2<T> _isometry; /** \var _isometry describes the 2d relative pose wrt _reference_frame*/
  };


  /** \typedef SensorParams2Vector<T> is the template version of a std::vector of pointer to SensorParams2<T> */
  template <typename T>
  using SensorParams2Vector = std::vector<SensorParams2<T>* >;

}

#endif
