/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _BASE_SAMPLE_HPP
#define _BASE_SAMPLE_HPP

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <stdlib.h>
#include <vector>

#include "typedefs.h"


namespace new_world_calibration{

  /**
   * BaseSample class.
   * describe how is composed a sample of the dataset.
   * It will be extended in 2d and 3d samples
   */
  template<typename T>
  class BaseSample{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    /**
     * @brief a constructor.
     * base constructor with no params.
     */
    BaseSample(){}

    /**
     * @brief a constructor.
     * @param ticks_, a VectorX<T>
     */
    BaseSample(const VectorX<T>& ticks_): _ticks(ticks_){}

    /**
     * @brief a destructor.
     */
    ~BaseSample(){}

    /**
     * @brief a set method for _ticks
     * @param ticks_, a VectorX<T>
     */
    void setTicks(const VectorX<T>& ticks_){_ticks = ticks_;}

    /**
     * @brief a get method for ticks
     * @return current value of _ticks
     */
    VectorX<T> ticks(){return _ticks;}

  protected:
    VectorX<T> _ticks; /** \var _ticks stores the current number of ticks recorder in this sample */
  };

}

#endif
