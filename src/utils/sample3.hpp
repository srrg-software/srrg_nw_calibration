/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _SAMPLE3_HPP_
#define _SAMPLE3_HPP_

#include "base_sample.hpp"
#include "odom_base_sample.hpp"

namespace new_world_calibration{

  /**
   * Sample3 class.
   * @see BaseSample()
   * extends BaseSample by adding a 3d Isometry.
   */
  template<typename T>
  class Sample3:public BaseSample<T>, public OdomBaseSample<T>{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    /**
     * @brief a constructor.
     * base constructor with no params.
     */
    Sample3(){}

    /**
     * @brief a constructor for tick based sample.
     * @param ticks_, VectorX<T> set of tick count
     * @param isometry_, Isometry3<T> representing the sensor motion
     */
    Sample3(const VectorX<T>& ticks_,
            const Isometry3<T>& isometry_): BaseSample<T>(ticks_), _isometry(isometry_){}

    /**
     * @brief a constructor for odom based sample.
     * @param ticks_, VectorX<T> set of tick count
     * @param isometry_, Isometry3<T> representing the sensor motion
     */
    Sample3(const Isometry2<T>& odom_,
            const Isometry3<T>& isometry_): OdomBaseSample<T>(odom_), _isometry(isometry_){}

    
    /**
     * @brief a destructor.
     */
    ~Sample3(){}

    /**
     * @brief a setter for the _isometry
     * @param isometry_, an Isometry3<T>
     */
    void setIsometry(const Isometry3<T>& isometry_) {this->_isometry = isometry_;}

    /**
     * @brief a getter for the _isometry
     * @return the 3d isometry stored in the sample
     */
    Isometry3<T> isometry(){return _isometry;}

  private:
    Isometry3<T> _isometry; /** \var _isometry stores the recorder 3d sensor motion */
  };


  /** \typedef Dataset3<T> is the template version of a std::vector of pointer to Sample3<T> */
  template <typename T>
  using Dataset3 = std::vector<Sample3<T>* >;

  /** \typedef Dataset3Vector<T> is the template version of a std::vector of pointer to Dataset3<T> */
  template <typename T>
  using Dataset3Vector = std::vector<Dataset3<T>* >;

}

#endif
