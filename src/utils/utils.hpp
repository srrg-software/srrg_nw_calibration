/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef UTILS_HPP
#define UTILS_HPP

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <iostream>



namespace new_world_calibration{

//!converts from Vector6<Derived1> to Isometry3<Derived1>
//!@param v: a vector (vx, vy, vz, qx, qy, qz) representing the transform.
//!(qx, qy, qz) are the imaginary part of a normalized quaternion, with qw>0.
//!@returns the Isometry corresponding to the transform described by v
//!
template <typename Derived1>
inline Eigen::Transform<Derived1,3,Eigen::Isometry> v2t(const Eigen::Matrix<Derived1, 6, 1>& v)
{
    Eigen::Transform<Derived1,3,Eigen::Isometry> T; //return an homogeneous transformation in a 3-dim space, aka 4x4
    T.template setIdentity();
    T.template translation() = v.template head<3>();
    Derived1 w = v.template block<3,1>(3,0).squaredNorm();
    if (w < (Derived1)1){
        w = sqrt(1 - w);
        Eigen::Quaternion<Derived1> q(w, v(3), v(4), v(5));
        T.template linear() = q.template toRotationMatrix();
    } else{
        Eigen::Matrix<Derived1, 3, 1> qv = v.template block<3,1>(3,0);
        qv.template normalize();
        Eigen::Quaternion<Derived1> q(0, qv(0), qv(1), qv(2));
        T.template linear() = q.template toRotationMatrix();
    }
    return T;
}

//!converts from Isometry3<Derived1 to Vector6<Derived1>
//!@param t: an isometry
//!@returns a vector (tx, ty, tz, qx, qy, qz) reptesenting the transform.
//!(qx, qy, qz) are the imaginary part of a normalized queternion, with qw>0.
template <typename Derived1>
inline Eigen::Matrix<Derived1, 6, 1> t2v(const Eigen::Transform<Derived1,3,Eigen::Isometry>& t){
    Eigen::Matrix<Derived1, 6, 1> v;
    v.template head<3>()= t.template translation();
    Eigen::Quaternion<Derived1> q(t.template linear());
    v(3) = q.template x();
    v(4) = q.template y();
    v(5) = q.template z();
    if (q.template w()<(Derived1)0)
        v.template block<3,1>(3,0) *= (Derived1)(-1);
    return v;
}


//!converts from Vector3<Derived1> to Isometry2<Derived1>
//!@param v: a vector (vx, vy, theta) representing the transform.
//!@returns the Isometry corresponding to the transform described by v
//!
template<typename Derived1>
inline Eigen::Transform<Derived1, 2, Eigen::Isometry> v2t(const Eigen::Matrix<Derived1, 3, 1> &v){

    Eigen::Transform<Derived1, 2, Eigen::Isometry> A;
    Eigen::Rotation2D<Derived1> rot(v(2));
    Eigen::Matrix<Derived1,2,2> rot_e(rot);
    Eigen::Matrix<Derived1, 2, 1> trans(v(0), v(1));

    A.template linear() = rot_e;
    A.template translation() = trans;
    return A;
}


//!converts from Isometry2<Derived1> to Vector3<Derived1>
//!@param A: the Isometry corresponding to the transform described by v
//!@returns a vector (vx, vy, theta) representing the transform.
//!
template<typename Derived1>
inline Eigen::Matrix<Derived1, 3, 1> t2v(const Eigen::Transform<Derived1, 2, Eigen::Isometry> &A){
    Eigen::Matrix<Derived1, 3, 1> t;
    t.template block(0,0,2,1) = A.template translation();
    t(2) = atan2(A(1,0),A(0,0));
    return t;
}

//!converts from Isometry3<Derived1> to Isometry2<Derived1> by extracting the 2D Transformation
//!@param B: the Isometry corresponding to the 3D transformation
//!@returns the 2D Isometry extracted from B
//!
template<typename Derived1>
inline Eigen::Transform<Derived1, 2, Eigen::Isometry> iso2(const Eigen::Transform<Derived1, 3, Eigen::Isometry> &B){
    Eigen::Transform<Derived1, 2, Eigen::Isometry> A;
    A.template translation() = B.template translation().head(2);
    Eigen::Matrix<Derived1,3,1> v = B.template linear().eulerAngles(0,1,2);
    Eigen::Rotation2D<Derived1> rot(v(2));
    Eigen::Matrix<Derived1,2,2> rot_e(rot);
    A.template linear() = rot_e;

    return A;
}

//!normalize between -pi and pi
//!@param theta: a radians
//!
template <typename T>
inline void normalizeTheta(T &theta){
    theta = atan2(sin(theta),cos(theta));
}

//!normalize the element of a VectorX between -pi and pi
//!@param v: a VectorX whose element has to be normalized
//!
template <typename T>
inline void normalizeTheta(Eigen::Matrix<T,Eigen::Dynamic,1>& v){
    for(unsigned int i=0; i < v.rows(); ++i)
        normalizeTheta(v(i));
}
\
//!normalize the element of a Vector2 between -pi and pi
//!@param v: a Vector2 whose element has to be normalized
//!
template <typename T>
inline void normalizeTheta(Eigen::Matrix<T,2,1>& v){
        normalizeTheta(v(0));
        normalizeTheta(v(1));
}


}

#endif //UTILS_HPP
