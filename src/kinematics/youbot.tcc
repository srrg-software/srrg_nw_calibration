/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

namespace new_world_calibration {

    template <typename T>
    void Youbot<T>::computeRobotStep(Vector3<T> &pose,
                                     const VectorX<T> &ticks,
                                     const VectorX<T> &params){

        T geom_factor = params(3);
        T w_FL = params(4);
        T w_FR = params(5);
        T w_BL = params(6);
        T w_BR = params(7);

        // factory kinematics
        //    T delta_longitudinal = (-ticks(0)*w_FL + ticks(1)*w_FR - ticks(2)*w_BL + ticks(3)*w_BR)/4;
        //    T delta_trasversal = (ticks(0)*w_FL + ticks(1)*w_FR - ticks(2)*w_BL - ticks(3)*w_BR)/4;
        //    pose(2) += (ticks(0)*w_FL + ticks(1)*w_FR + ticks(2)*w_BL + ticks(3)*w_BR)/(4*geom_factor);

        // vrep compliant kinematics
        T delta_longitudinal = (-ticks(0)*w_FL - ticks(1)*w_FR - ticks(2)*w_BL - ticks(3)*w_BR)/4;
        T delta_trasversal = (ticks(0)*w_FL - ticks(1)*w_FR - ticks(2)*w_BL + ticks(3)*w_BR)/4;
        pose(2) += (ticks(0)*w_FL - ticks(1)*w_FR + ticks(2)*w_BL - ticks(3)*w_BR)/(4*geom_factor);

        normalizeTheta(pose(2));
        pose(0) = pose(0) + delta_longitudinal*cos(pose(2)) - delta_trasversal*sin(pose(2));
        pose(1) = pose(1) + delta_longitudinal*sin(pose(2)) + delta_trasversal*cos(pose(2));
    }


}
