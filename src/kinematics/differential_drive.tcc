/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

namespace new_world_calibration{

    template <typename T>
    inline T sinThetaOverTheta(T theta) {
        if (fabs(theta)<EPSILON)
            return 1;
        return sin(theta)/theta;
    }

    template <typename T>
    inline T oneMinisCosThetaOverTheta(T theta) {
        if (fabs(theta)<EPSILON)
            return 0;
        return (1.0f-cos(theta))/theta;
    }

#define _USE_TAYLOR_EXPANSION_


    template <typename T>
    void computeThetaTerms(T& sin_theta_over_theta,
                           T& one_minus_cos_theta_over_theta,
                           T theta) {
#ifdef _USE_TAYLOR_EXPANSION_
        const T cos_coeffs[]={0., 0.5 ,  0.    ,   -1.0/24.0,   0.    , 1.0/720.0};
        const T sin_coeffs[]={1., 0.  , -1./6. ,      0.    ,   1./120, 0.   };
        // evaluates the taylor expansion of sin(x)/x and (1-cos(x))/x,
        // where the linearization point is x=0, and the functions are evaluated
        // in x=theta
        sin_theta_over_theta=0;
        one_minus_cos_theta_over_theta=0;
        T theta_acc=1;
        for (uint8_t i=0; i<6; i++) {
                if (i&0x1)
                    one_minus_cos_theta_over_theta+=theta_acc*cos_coeffs[i];
                else
                    sin_theta_over_theta+=theta_acc*sin_coeffs[i];
                theta_acc*=theta;
            }
#else
        sin_theta_over_theta=sinThetaOverTheta(theta);
        one_minus_cos_theta_over_theta=oneMinisCosThetaOverTheta(theta) ;
#endif
    }



    template <typename T>
    void DifferentialDrive<T>::computeRobotStep(Vector3<T>& pose,
                                                const VectorX<T>& ticks,
                                                const VectorX<T>& params){

        T delta_l = ticks(0)*params(0);
        T delta_r = ticks(1)*params(1);
        T baseline = params(2);

        T delta_plus=delta_r+delta_l;
        T delta_minus=delta_r-delta_l;
        T dth=delta_minus/baseline;
        T one_minus_cos_theta_over_theta, sin_theta_over_theta;
        computeThetaTerms(sin_theta_over_theta, one_minus_cos_theta_over_theta, dth);
        T dx=.5*delta_plus*sin_theta_over_theta;
        T dy=.5*delta_plus*one_minus_cos_theta_over_theta;

        //apply the increment to the previous estimate
        T s=sin(pose(2));
        T c=cos(pose(2));
        pose(0) +=c*dx-s*dy;
        pose(1) +=s*dx+c*dy;
        pose(2) +=dth;
        normalizeTheta(pose(2));
    }

    template <typename T>
    void DifferentialDrive<T>::predictTicks(Vector2<T>& predicted_ticks,
                      const Isometry2<T>& platform_motion,
                      const VectorX<T>& params){

        Vector3<T> variations = t2v(platform_motion);
        T rho = 0;
        T Dx = variations(0);
        normalizeTheta(variations(2));

        if(sin(variations(2)) > 1e-8) {
            T R = Dx/sin(variations(2));
            //double R = (pow(Dx,2) + pow(variations(1),2))/2*variations(1);
            rho = R*variations(2);
        } else {
            rho = std::copysign(1.0,variations(0))*fabs(sqrt(pow(variations(0),2) + pow(variations(1),2)));
        }

        predicted_ticks.setZero();
        predicted_ticks(0) = 1/params(0) * (rho - (params(2)/2)*variations(2));
        predicted_ticks(1) = 1/params(1) * (rho + (params(2)/2)*variations(2));

    }

}
