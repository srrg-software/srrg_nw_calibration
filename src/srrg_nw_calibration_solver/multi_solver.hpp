/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MULTI_SOLVER2_HPP
#define MULTI_SOLVER2_HPP

#include "basesolver.hpp"
#include "solver3.hpp"
#include "solver2.hpp"

namespace new_world_calibration{

  /**
   * MultiSolver class.
   * @see BaseSolver()
   * @see Solver2()
   * @see Solver3()
   * solver implementation for 2d/3d problems
   * manage a set of 2d/3d solvers collecting information
   * and solving a single, heterogeneous LS problem
   */
  template <typename T>
  class MultiSolver : public BaseSolver<T>{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    /**
     * @brief a constructor
     * @param num2d_sensor_, number of 2d sensor involved
     * @param num3d_sensor_, number of 3d sensor involved
     */
    MultiSolver(const int& num2d_sensor_, const int& num3d_sensor_) : _num2d_sensor(num2d_sensor_), _num3d_sensor(num3d_sensor_){
      this->_iterations = 10;
      this->_epsilon = 1e-4;
      _information_weights2 = std::vector<int>(num2d_sensor_,1);
      _information_weights3 = std::vector<int>(num3d_sensor_,1);
    }

    /**
     * @brief a destructor
     */
    ~MultiSolver(){}

    /**
     * @brief setter for SensorParams2Vector
     * @param sensor_params_2_, vector of SensorParams2 pointers
     */
    void setSensorParams2(const SensorParams2Vector<T>& sensor_params_2_){
        if(sensor_params_2_.size() == _num2d_sensor)
//            _sensor_params_2 = sensor_params_2_;
            copyParams2Vector(sensor_params_2_);
        else std::runtime_error("invalid sensor_params_2 size");
    }

    /**
     * @brief setter for SensorParams3Vector
     * @param sensor_params_3_, vector of SensorParams3 pointers
     */
    void setSensorParams3(const SensorParams3Vector<T>& sensor_params_3_){
        if(sensor_params_3_.size() == _num3d_sensor)
//            _sensor_params_3 = sensor_params_3_;
            copyParams3Vector(sensor_params_3_);
        else std::runtime_error("invalid sensor_params_3 size");
    }

   /**
   * @brief setter for OdomParams, overloads the baseSolver method
   * this overloading is made to gain ownership of the _odom_params
   * @param odom_params_, pointer to OdomParams<T>
   */
   void setOdomParams(const OdomParams<T>* odom_params_){this->_odom_params = new OdomParams<T>(odom_params_);}

    /**
     * @brief setter for LS iterations
     * @param iterations_, integer of required iterations
     */
    void setIterations(const int& iterations_){_iterations = iterations_;}

   /**
   * @brief setter for information weights vector, one for each 2d solver/sensor
   * @param information_weights_, vector of integer
   */
    void setInformationWeights2(const std::vector<int>& information_weights2_){_information_weights2 = information_weights2_;}

   /**
   * @brief setter for information weights vector, one for each 3d solver/sensor
   * @param information_weights_, vector of integer
   */
    void setInformationWeights3(const std::vector<int>& information_weights3_){_information_weights3 = information_weights3_;}

    /**
     * @brief getter for _odom_params
     * @return a pointer to OdomParams<T>
     */
//    OdomParams<T>* odomParams(){return this->_odom_params;}

    /**
     * @brief getter for _sensor_params_2
     * @return a pointer to SensorParams2Vector<T>
     */
    SensorParams2Vector<T> sensorParams2(){return _sensor_params_2;}

    /**
     * @brief getter for _sensor_params_3
     * @return a pointer to SensorParams3Vector<T>
     */
    SensorParams3Vector<T> sensorParams3(){return _sensor_params_3;}

    /**
     * @brief initialization method for the MultiSolver
     * this methods instantiate the required Solver2 and Solver3
     * and set to each of them the proper Params, Kinematics, Epsilon
     */
    void init();

    /**
    * @brief reset/clear method for the MultiSolver
    * this methods reset all the content of the multisolver.
    * It is called at the beginning of the init method
    */
    void reset();

    /**
     * @brief the main function, solve the heterogeneous full LS problem
     * @param datasets_2, a vector of pointer to 2d Dataset
     * @param datasets_3, a vector of pointer to 3d Dataset
     */
    void solve(Dataset2Vector<T> datasets_2, Dataset3Vector<T> datasets_3);

    /**
     * @brief update all the Parameters by calling the respective update functions
     * @param deltaX, VectorX of increment, aka solution of the LS full problem
     */
    void updateParameters(const VectorX<T>& deltaX);

    /**
     * @brief print all the Params by calling the respective printStatus functions
     */
    void printParams();

  protected:

    /**
    * @brief copy method for SensorParams2Vector
    * gain ownership of the sensor2_params
    * @param sensor_params, 2d SensorParams2Vector<T>
    */
    void copyParams2Vector(const SensorParams2Vector<T> sensor_params);

    /**
    * @brief copy method for SensorParams3Vector
    * gain ownership of the sensor3_params
    * @param sensor_params, 3d SensorParams2Vector<T>
    */
    void copyParams3Vector(const SensorParams3Vector<T> sensor_params);

  private:
    int _num2d_sensor; /** \var number of 2d sensor involved */
    int _num3d_sensor; /** \var number of 3d sensor involved */
    int _total_parameter_size; /** \var total number of parameters involved */

    Solver2Vector<T> _solvers_2; /** \var vector of Solver2 */
    Solver3Vector<T> _solvers_3; /** \var vector of Solver3 */
    SensorParams2Vector<T> _sensor_params_2; /** \var vector of 2d params */
    SensorParams3Vector<T> _sensor_params_3; /** \var vector of 3d params */

    Dataset2Vector<T> _datasets_2; /** \var current vector of 2d dataset */
    Dataset3Vector<T> _datasets_3; /** \var current vector of 3d dataset */

    int _iterations; /** \var LS iterations required */

    std::vector<int> _information_weights2; /** \var vector of integer, one per 2d sensor/solver. They will be multiplied by the Omegas of the LS */
    std::vector<int> _information_weights3; /** \var vector of integer, one per 3d sensor/solver. They will be multiplied by the Omegas of the LS */

  };

}

#include "multi_solver.tcc"

#endif // MULTI_SOLVER2_HPP
