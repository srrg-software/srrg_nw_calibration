/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

namespace new_world_calibration{

    template <typename T>
    void MultiSolver<T>::init(){

        reset();

        std::cerr<<"[Multi_Solver]: initializing the solvers "<<std::endl;
        for(size_t i=0; i < _num2d_sensor; ++i){
                Solver2<T>* solver2 = new Solver2<T>;
                solver2->setOdomParams(this->_odom_params);
                solver2->setKinematics(this->_robot);
                solver2->setEpsilon(this->_epsilon);
                solver2->setSensorParams2(_sensor_params_2.at(i));
                solver2->setInformationWeight(_information_weights2.at(i));
                _solvers_2.push_back(solver2);
            }
        for(size_t i=0; i < _num3d_sensor; ++i){
                Solver3<T>* solver3 = new Solver3<T>;
                solver3->setOdomParams(this->_odom_params);
                solver3->setKinematics(this->_robot);
                solver3->setEpsilon(this->_epsilon);
                solver3->setSensorParams3(_sensor_params_3.at(i));
                solver3->setInformationWeight(_information_weights3.at(i));
                _solvers_3.push_back(solver3);
            }

        _total_parameter_size = _num2d_sensor*SensorParams2Number + _num3d_sensor*SensorParams3Number + this->_odom_params->size();
        //init H and b of multiSolver
        this->_H = MatrixX<T>::Zero(_total_parameter_size, _total_parameter_size);
        this->_b = VectorX<T>::Zero(_total_parameter_size);

    }

    template<typename T>
    void MultiSolver<T>::reset(){
        for(typename Solver2Vector<T>::iterator it = _solvers_2.begin(); it != _solvers_2.end(); ++it){
            (*it)->~Solver2();
            //delete (*it);
        }
        _solvers_2.clear();

        for(typename Solver3Vector<T>::iterator it = _solvers_3.begin(); it != _solvers_3.end(); ++it){
            (*it)->~Solver3();
            //delete (*it);
        }
        _solvers_3.clear();
    }

    template <typename T>
    void MultiSolver<T>::solve(Dataset2Vector<T> datasets_2, Dataset3Vector<T> datasets_3){
        //set datasets
        for(size_t i=0; i < _num2d_sensor; ++i){
                _solvers_2[i]->setDataset2(datasets_2[i]);
            }
        for(size_t i=0; i < _num3d_sensor; ++i){
                _solvers_3[i]->setDataset3(datasets_3[i]);
            }

        int odom_param_size = this->_odom_params->size();

        for(size_t iteration = 0; iteration < _iterations; ++iteration){

                for(size_t i=0; i < _num2d_sensor; ++i){
                        _solvers_2[i]->computeMatrices();
                    }
                for(size_t i=0; i < _num3d_sensor; ++i){
                        _solvers_3[i]->computeMatrices();
                    }

                this->_b.setZero();
                this->_H.setZero();
                this->_chi2 = 0;

                for(size_t i=0; i < _num2d_sensor; ++i){
                        VectorX<T> b = _solvers_2[i]->b();
                        //   std::cerr<<"[solver2] b: "<<b.transpose()<<std::endl;
                        MatrixX<T> H = _solvers_2[i]->H();
                        //sensor-sensor
                        this->_b.block(i*SensorParams2Number,0,SensorParams2Number,1) =
                                b.head(SensorParams2Number);
                        this->_H.block(i*SensorParams2Number,i*SensorParams2Number, SensorParams2Number, SensorParams2Number) =
                                H.block(0, 0, SensorParams2Number, SensorParams2Number);

                        //sensor-odom
                        this->_H.block(_total_parameter_size - odom_param_size, i*SensorParams2Number, odom_param_size, SensorParams2Number) =
                                H.block(SensorParams2Number, 0, odom_param_size, SensorParams2Number);
                        this->_H.block(i*SensorParams2Number, _total_parameter_size - odom_param_size, SensorParams2Number, odom_param_size) =
                                H.block(0, SensorParams2Number, SensorParams2Number, odom_param_size);

                        //odom
                        this->_H.block(_total_parameter_size - odom_param_size, _total_parameter_size - odom_param_size, odom_param_size, odom_param_size) +=
                                H.block(SensorParams2Number, SensorParams2Number, odom_param_size, odom_param_size);
                        this->_b.tail(odom_param_size) += b.tail(odom_param_size);

                        this->_chi2 += _solvers_2[i]->chi2();
                    }

                int shift = _num2d_sensor*SensorParams2Number;
                for(size_t i=0; i < _num3d_sensor; ++i){
                        VectorX<T> b = _solvers_3[i]->b();
                        MatrixX<T> H = _solvers_3[i]->H();
                        //sensor-sensor
                        this->_b.block(i*SensorParams3Number + shift,0,SensorParams3Number,1) =
                                b.head(SensorParams3Number);
                        this->_H.block(i*SensorParams3Number+shift,i*SensorParams3Number+shift, SensorParams3Number, SensorParams3Number) =
                                H.block(0, 0, SensorParams3Number, SensorParams3Number);

                        //sensor-odom
                        this->_H.block(_total_parameter_size - odom_param_size, i*SensorParams3Number+shift, odom_param_size, SensorParams3Number) =
                                H.block(SensorParams3Number, 0, odom_param_size, SensorParams3Number);
                        this->_H.block(i*SensorParams3Number+shift, _total_parameter_size - odom_param_size, SensorParams3Number, odom_param_size) =
                                H.block(0, SensorParams3Number, SensorParams3Number, odom_param_size);

                        //odom
                        this->_H.block(_total_parameter_size - odom_param_size, _total_parameter_size - odom_param_size, odom_param_size, odom_param_size) +=
                                H.block(SensorParams3Number, SensorParams3Number, odom_param_size, odom_param_size);
                        this->_b.tail(odom_param_size) += b.tail(odom_param_size);

                        this->_chi2 += _solvers_3[i]->chi2();
                    }

                //solve the system
                VectorX<T> deltaX = (this->_H).ldlt().solve(-this->_b);

                //update parameters
                updateParameters(deltaX);

                std::cerr<<"["<<iteration+1<<"/"<<_iterations<<"] chi^2: "<<this->_chi2<<std::endl;
            }

        printParams();
    }

    template <typename T>
    void MultiSolver<T>::updateParameters(const VectorX<T>& deltaX){

        for(size_t i=0; i < _num2d_sensor; ++i){
                Vector3<T> delta2 = deltaX.block(i*SensorParams2Number, 0, SensorParams2Number,1);
                Isometry2<T> delta = v2t(delta2);
                _sensor_params_2[i]->updateParams(delta);
            }
        int shift = _num2d_sensor*SensorParams2Number;
        for(size_t i=0; i < _num3d_sensor; ++i){
                Vector6<T> delta3 = deltaX.block(i*SensorParams3Number+shift, 0, SensorParams3Number,1);
                Isometry3<T> delta = v2t(delta3);
                _sensor_params_3[i]->updateParams(delta);
            }

        this->_odom_params->updateParams(deltaX.tail(this->_odom_params->size()));

    }

    template <typename T>
    void MultiSolver<T>::printParams(){
        this->_odom_params->printStatus();
        for(size_t i=0; i < _num2d_sensor; ++i){
                _sensor_params_2[i]->printStatus();
            }
        for(size_t i=0; i < _num3d_sensor; ++i){
                _sensor_params_3[i]->printStatus();
            }
    }

    template <typename T>
    void MultiSolver<T>::copyParams2Vector(const SensorParams2Vector<T> sensor_params){
        if(_sensor_params_2.size() > 0)
            _sensor_params_2.clear();

//        _sensor_params_2.resize(sensor_params.size());
        typename SensorParams2Vector<T>::const_iterator it = sensor_params.begin();
        for(it; it!=sensor_params.end(); ++it){
            SensorParams2<T>* curr_sensor_params = new SensorParams2<T>(*it);
            _sensor_params_2.push_back(curr_sensor_params);
        }
    }

    template <typename T>
    void MultiSolver<T>::copyParams3Vector(const SensorParams3Vector<T> sensor_params){
        if(_sensor_params_3.size() > 0)
            _sensor_params_3.clear();

//        _sensor_params_3.resize(sensor_params.size());
        typename SensorParams3Vector<T>::const_iterator it = sensor_params.begin();
        for(it; it!=sensor_params.end(); ++it){
            SensorParams3<T>* curr_sensor_params = new SensorParams3<T>(*it);
            _sensor_params_3.push_back(curr_sensor_params);
        }
    }

}
