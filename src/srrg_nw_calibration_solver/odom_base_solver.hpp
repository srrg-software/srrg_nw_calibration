/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _ODOM_BASE_SOLVER_HPP
#define _ODOM_BASE_SOLVER_HPP

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include "../utils/utils.hpp"
#include "../utils/sample3.hpp"
#include "../utils/sample2.hpp"
#include "basesolver.hpp"
#include "../utils/sensor_params2.hpp"
#include "../utils/sensor_params3.hpp"
#include "../utils/typedefs.h"

#include "prior.hpp"

namespace new_world_calibration{

  /**
   * OdomBaseSolver class.
   * base structure of a solver, contains all the shared matrices and vectors
   * with the respective getter for the odom based calibration
   */
  template<typename T>
  class OdomBaseSolver{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    /**
     * @brief a constructor
     */
    OdomBaseSolver(){_information_weight = 1;}

    /**
     * @brief a destructor
     */
    ~OdomBaseSolver(){}

    /**
     * @brief setter for epsilon
     * this epsilon value regulate the least square Jacobian computation
     * @param epsilon_
     */
    void setEpsilon(const T& epsilon_){_epsilon = epsilon_;}

    /**
     * @brief setter for information weight
     * this weight will be multiplied by the Omega in the computation of H and b matrices
     * @param information_weight_
     */
    void setInformationWeight(const int& information_weight_){_information_weight = information_weight_;}

    /**
     * @brief getter for _H matrix
     * @return the "Hessian", aka Approximation of information matrix of the LS problem
     */
    MatrixX<T>  H()    {return _H;}

    /**
     * @brief getter for _b vector
     * @return vector b of the least squares problem
     */
    VectorX<T>  b()    {return _b;}

    /**
     * @brief getter for current Jacobian
     * @return [mxn] matrix containing the Jacobian
     */
    MatrixX<T>  J()    {return _J;}

    /**
     * @brief getter for error
     * @return [mx1] vector containing the error
     */
    MatrixX<T>  e()    {return _e;}

    /**
     * @brief getter for chi2
     * @return the chi^2
     */
    T           chi2() {return _chi2;}


  protected:
    MatrixX<T> _H; /** \var _H is the "Hessian" of the LS */
    VectorX<T> _b; /** \var _b is the b vector of the LS used to solve the linear system*/
    MatrixX<T> _J; /** \var current Jacobian */
    VectorX<T> _e; /** \var current error */

    T _epsilon; /** \var epsilon use to compute jacobian */
    T _chi2; /** \var current chi^2 */
    int _information_weight; /** \var integer multiplied to the Omega to weight the information. The higher, the more informative will be considered the solver. default = 1 */

  };


}

#endif
