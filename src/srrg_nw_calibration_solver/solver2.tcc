/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

namespace new_world_calibration{

    template <typename T>
    void Solver2<T>::computeMatrices(){
        //reset && resize all the Matrices
        int total_number_of_parameters = SensorParams2Number + this->_odom_params->size();
        this->_J = MatrixX<T>::Zero(Error2Dimension, total_number_of_parameters);
        this->_e = VectorX<T>::Zero(Error2Dimension);
        this->_H = MatrixX<T>::Zero(total_number_of_parameters, total_number_of_parameters);
        this->_b = VectorX<T>::Zero(total_number_of_parameters);
        this->_chi2 = 0;

        MatrixX<T> Omega = MatrixX<T>::Identity(Error2Dimension, Error2Dimension);
        Omega *= this->_information_weight;

        for(typename Dataset2<T>::iterator sample = _dataset->begin(); sample != _dataset->end(); ++sample){
                //compute error and Jacobian
                this->_e = computeError(*sample);
                this->_J = computeJacobian(*sample);
                //update chi2
                this->_chi2 += this->_e.squaredNorm();

                //compute H matrix and b vector
                this->_H += this->_J.transpose() * Omega * this->_J;
                this->_b += this->_J.transpose() * Omega * this->_e;
            }

        this->_chi2 /= _dataset->size();
    }



    template <typename T>
    MatrixX<T> Solver2<T>::computeJacobian(Sample2<T>* sample){
        MatrixX<T> J = MatrixX<T>::Zero(Error2Dimension, SensorParams2Number + this->_odom_params->size());

        for(unsigned int i=0; i < SensorParams2Number; ++i){
                J.col(i) = computeError(sample, i, +1) - computeError(sample, i, -1);
            }
        for(unsigned int i= SensorParams2Number; i < SensorParams2Number + this->_odom_params->size() ; ++i){
                J.col(i) = computeError(sample, i, +1) - computeError(sample, i, -1);
            }
        return .5*J/this->_epsilon;
    }



    template <typename T>
    MatrixX<T> Solver2<T>::computeError(Sample2<T>* sample, const int& increment, const int& sign){

      OdomParams<T>* incremented_odom_params = new OdomParams<T>(this->_odom_params);
      SensorParams2<T>* incremented_sensor_params = new SensorParams2<T>(_sensor_params);

      //apply the increment if required
      if(increment != -1){
          if(increment < SensorParams2Number){
              Vector3<T> increment_v(Vector3<T>::Zero());
              increment_v(increment) = this->_epsilon*sign;
              Isometry2<T> increment_iso = v2t(increment_v);
              incremented_sensor_params->updateParams(increment_iso);
            }
          else{
              VectorX<T> increment_v(VectorX<T>::Zero(this->_odom_params->size()));
              increment_v(increment - SensorParams2Number) = this->_epsilon*sign;
              incremented_odom_params->updateParams(increment_v);
            }
        }

      //compute odom increment
      Vector3<T> robot_pose(Vector3<T>::Zero());
      this->_robot->computeRobotStep(robot_pose, sample->ticks(), incremented_odom_params->odomParams());
      Isometry2<T> robot_iso = v2t((Vector3<T>)robot_pose);
      //apply sensor transform
      Isometry2<T> sensor_iso = incremented_sensor_params->isometry().inverse() * robot_iso * incremented_sensor_params->isometry();
      //compute error between isometries
      Isometry2<T> error = sample->isometry().inverse() * sensor_iso;
      return t2v(error);
    }

}
