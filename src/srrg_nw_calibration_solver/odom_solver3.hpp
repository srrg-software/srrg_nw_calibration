/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _ODOM_SOLVER3_HPP
#define _ODOM_SOLVER3_HPP

#include "odom_base_solver.hpp"

namespace new_world_calibration{

  /**
   * OdomSolver3 class.
   * solver implementation for 3d problems, stores, computes and returns all the
   * needed matrices
   */
  template<typename T>
  class OdomSolver3 : public OdomBaseSolver<T>{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    /**
     * @brief a constructor
     */
    OdomSolver3(){}

    /**
     * @brief a destructor
     */
    ~OdomSolver3(){};

    /**
     * @brief setter of SensorParams3
     * @param sensor_params_, pointer to SensorParams3<T>
     */
    void setSensorParams3(SensorParams3<T>* sensor_params_);

    /**
     * @brief setter of Dataset3
     * @param dataset_, pointer to Dataset3<T>
     */
    void setDataset3(Dataset3<T>* dataset_){_dataset = dataset_;}

    /**
     * @brief getter of _sensor_params
     * @return pointer to SensorParams3
     */
    SensorParams3<T>* sensor_params(){return _sensor_params;}

    /**
     * @brief this methods compute the matrices needed for LS computation
     * practically it computes the H and b matrices of a 3d sub-problem
     */
    void computeMatrices();

    /**
     * @brief compute the jacobian for LS
     * @param sample, a pointer to Sample3
     * @return a MatrixX, i.e. the Jacobian
     */
    MatrixX<T> computeJacobian(Sample3<T>* sample);

    /**
     * @brief compute the error for LS
     * @param sample, pointer to Sample3
     * @param increment, index of the parameters to be incremented, default -1
     * @param sign, sign of increment to apply, default 0
     * @return a MatrixX, i.e. the error
     */
    VectorX<T> computeError(Sample3<T>* sample, const int& increment = -1, const int& sign = 0);

    /**
     * @brief this method adds a prior to the LS
     * as default operation, a prior on the z is applied, i.e. z
     * will stay fixed at the initial value
     * @param prior_mean, Isometry3
     * @param prior_info, Matrix6
     */
    void addPrior(const Isometry3<T>& prior_mean,
                  const Matrix6<T>& prior_info);

  private:
    SensorParams3<T>* _sensor_params; /** \var pointer to a SensorParams3 set. owned by MultiSolver */
    Dataset3<T>* _dataset; /** \var pointer to a Dataset3. owned by MultiSolver */
    SolverPriorVector<T> _priors; /** \var vector of priors. */
  };

  /** \typedef Solver3Vector<T> is the template version of a std::vector of pointer to Solver3<T> */
  template <typename T>
  using OdomSolver3Vector = std::vector<OdomSolver3<T>* >;

}

#include "odom_solver3.cpp"

#endif
