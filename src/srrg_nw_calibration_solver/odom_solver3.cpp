/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

namespace new_world_calibration{


    template <typename T>
    void OdomSolver3<T>::setSensorParams3(SensorParams3<T>* sensor_params_){
        _sensor_params = sensor_params_;
        _sensor_params->printStatus();
        //add prior on z
        Matrix6<T> info;
        info.setZero();
        info(2,2)=1e2;
        Isometry3<T> mean;
        mean.setIdentity();
        Vector6<T> params = t2v(sensor_params_->isometry());
        mean.translation().z()= params(2);//z

        addPrior(mean, info);
    }

    template <typename T>
    void OdomSolver3<T>::computeMatrices(){
        //reset && resize all the Matrices
        int total_number_of_parameters = SensorParams3Number;
        this->_J = MatrixX<T>::Zero(Error3Dimension, total_number_of_parameters);
        this->_e = VectorX<T>::Zero(Error3Dimension);
        this->_H = MatrixX<T>::Zero(total_number_of_parameters, total_number_of_parameters);
        this->_b = VectorX<T>::Zero(total_number_of_parameters);
        this->_chi2 = 0;

        MatrixX<T> Omega = MatrixX<T>::Identity(Error3Dimension, Error3Dimension);
        Omega *= this->_information_weight;

        for(typename Dataset3<T>::iterator sample = _dataset->begin(); sample != _dataset->end(); ++sample){
                //compute error and Jacobian
                this->_e = computeError(*sample);
                this->_J = computeJacobian(*sample);
                //update chi2
                this->_chi2 += this->_e.squaredNorm();

                //compute H matrix and b vector
                this->_H += this->_J.transpose() * Omega * this->_J;
                this->_b += this->_J.transpose() * Omega * this->_e;
            }

        for(size_t i = 0; i < _priors.size(); i++){
                Vector6<T> prior_e =  _priors[i].error();
                Matrix6<T> prior_J_6 = _priors[i].jacobian();
                MatrixX<T> prior_J(MatrixX<T>::Zero(SensorParams3Number, total_number_of_parameters));
                prior_J.block(0,0,6,6) = prior_J_6;
                const Matrix6<T>& prior_omega  = _priors[i].omega();
                float prior_error = prior_e.transpose()* prior_omega * prior_e;
                this->_chi2 += prior_error;
                this->_H += prior_J.transpose() * prior_omega * prior_J;
                this->_b += prior_J.transpose() * prior_omega * prior_e;
            }

        this->_chi2 /= _dataset->size();
    }

    template <typename T>
    MatrixX<T> OdomSolver3<T>::computeJacobian(Sample3<T>* sample){
      MatrixX<T> J = MatrixX<T>::Zero(Error3Dimension, SensorParams3Number);

        for(unsigned int i=0; i < SensorParams3Number; ++i){
                J.col(i) = computeError(sample, i, +1) - computeError(sample, i, -1);
            }

        return .5*J/this->_epsilon;
    }

    template <typename T>
    VectorX<T> OdomSolver3<T>::computeError(Sample3<T>* sample, const int& increment, const int& sign){

        SensorParams3<T>* incremented_sensor_params = new SensorParams3<T>(_sensor_params);

        //apply the increment if required
        if(increment != -1){
                if(increment < SensorParams3Number){
                        Vector6<T> increment_v(Vector6<T>::Zero());
                        increment_v(increment) = this->_epsilon*sign;
                        Isometry3<T> increment_iso = v2t(increment_v);
                        incremented_sensor_params->updateParams(increment_iso);
                    }
                else
		  std::runtime_error("out of params number");
                    
            }

        //compute odom increment
	Vector3<T> sample_odometry_vector = t2v(sample->odom());
        Vector6<T> robot_pose6(Vector6<T>::Zero());
        robot_pose6.head(2) = sample_odometry_vector.head(2);
        AngleAxis<T> lin_part(sample_odometry_vector(2), Vector3<T>::UnitZ());
        Quaternion<T> q(lin_part);
        robot_pose6.tail(3) = Vector3<T>(q.x(), q.y(), q.z());

        Isometry3<T> robot_iso = v2t(robot_pose6);
        //apply sensor transform
        Isometry3<T> sensor_iso = incremented_sensor_params->isometry().inverse() * robot_iso * incremented_sensor_params->isometry();
        //compute error between isometries
        Isometry3<T> error = sample->isometry().inverse() * sensor_iso;
        return t2v(error);
    }

    template <typename T>
    void OdomSolver3<T>::addPrior(const Isometry3<T>& prior_mean,
                              const Matrix6<T>& prior_info){
        SolverPrior<T> prior(prior_mean,
                             prior_info,
                             _sensor_params);
        _priors.push_back(prior);
    }



}
