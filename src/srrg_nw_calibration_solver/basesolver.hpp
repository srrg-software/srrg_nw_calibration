/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _BASE_SOLVER_HPP
#define _BASE_SOLVER_HPP

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include "../utils/utils.hpp"
#include "../utils/sample3.hpp"
#include "../utils/sample2.hpp"
#include "../utils/odom_params.hpp"
#include "../utils/sensor_params2.hpp"
#include "../utils/sensor_params3.hpp"
#include "../kinematics/robots.hpp"
#include "../utils/typedefs.h"

#include "prior.hpp"

namespace new_world_calibration{

  const int SensorParams2Number = 3; /** \var SensorParams2Number is used to define as 3 the std number of params for 2d sensor*/
  const int SensorParams3Number = 6; /** \var SensorParams2Number is used to define as 6 the std number of params for 3d sensor*/
  const int Error2Dimension = 3; /** \var SensorParams2Number is used to define as 3 the standard dimension of error for 2d problems*/
  const int Error3Dimension = 6; /** \var SensorParams2Number is used to define as 6 the standard dimension of error for 3d problems*/

  /**
   * BaseSolver class.
   * base structure of a solver, contains all the shared matrices and vectors
   * with the respective getter
   */
  template<typename T>
  class BaseSolver{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    /**
     * @brief a constructor
     */
    BaseSolver(){_information_weight = 1;}

    /**
     * @brief a destructor
     */
    ~BaseSolver(){}

    /**
     * @brief setter for Kinematics
     * @param robot_, pointer to Kinematics
     */
    void setKinematics(Kinematics<T>* robot_){_robot = robot_;}

    /**
     * @brief setter for OdomParams
     * @param odom_params_, pointer to OdomParams<T>
     */
    void setOdomParams(OdomParams<T>* odom_params_){_odom_params = odom_params_;}

    /**
     * @brief setter for epsilon
     * this epsilon value regulate the least square Jacobian computation
     * @param epsilon_
     */
    void setEpsilon(const T& epsilon_){_epsilon = epsilon_;}

    /**
     * @brief setter for information weight
     * this weight will be multiplied by the Omega in the computation of H and b matrices
     * @param information_weight_
     */
    void setInformationWeight(const int& information_weight_){_information_weight = information_weight_;}

    /**
     * @brief getter for _robot
     * @return a pointer to Kinematics<T>
     */
    Kinematics<T>* robot() {return _robot;}

    /**
     * @brief getter for odom_params
     * @return a pointer to OdomParams<T>
     */
    OdomParams<T>* odomParams() {return _odom_params;}

    /**
     * @brief getter for _H matrix
     * @return the "Hessian", aka Approximation of information matrix of the LS problem
     */
    MatrixX<T>  H()    {return _H;}

    /**
     * @brief getter for _b vector
     * @return vector b of the least squares problem
     */
    VectorX<T>  b()    {return _b;}

    /**
     * @brief getter for current Jacobian
     * @return [mxn] matrix containing the Jacobian
     */
    MatrixX<T>  J()    {return _J;}

    /**
     * @brief getter for error
     * @return [mx1] vector containing the error
     */
    MatrixX<T>  e()    {return _e;}

    /**
     * @brief getter for chi2
     * @return the chi^2
     */
    T           chi2() {return _chi2;}


  protected:
    MatrixX<T> _H; /** \var _H is the "Hessian" of the LS */
    VectorX<T> _b; /** \var _b is the b vector of the LS used to solve the linear system*/
    MatrixX<T> _J; /** \var current Jacobian */
    VectorX<T> _e; /** \var current error */

    T _epsilon; /** \var epsilon use to compute jacobian */
    T _chi2; /** \var current chi^2 */
    int _information_weight; /** \var integer multiplied to the Omega to weight the information. The higher, the more informative will be considered the solver. default = 1 */


    Kinematics<T>* _robot; /** \var a pointer to Kinematics */
    OdomParams<T>* _odom_params; /** \var pointer to odometry paramter set */
  };


}

#endif
