/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PRIOR_HPP
#define PRIOR_HPP

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include "../utils/utils.hpp"

#include <vector>

#include "../utils/typedefs.h"
#include "../utils/sensor_params3.hpp"

namespace new_world_calibration{

    /**
     * SolverPrior struct.
     * @see Solver3()
     * this struct can be used to put priors on the Solver3
     * e.g. keep the z at a fixed value
     */
    template <typename T>
    struct SolverPrior{
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

        /**
         * @brief a constructor
         * @param camera_offset_mean, Isometry3<T>
         * @param camera_offset_info, Matrix6<T>
         * @param camera_offset, pointer to SensorParams3<T>
         */
        SolverPrior(const Isometry3<T>& camera_offset_mean,
                    const Matrix6<T>& camera_offset_info,
                    SensorParams3<T>* camera_offset) :
            _camera_offset_mean(camera_offset_mean),
            _camera_offset_info(camera_offset_info) {
            _inverse_camera_offset_mean = _camera_offset_mean.inverse();
            _camera_offset = camera_offset;
        }

        /**
         * @brief this function computes the omega for the LS computation with the prior
         * @return a MatrixX, the omega
         */
        inline MatrixX<T> omega() const{
            Matrix3<T> R = _inverse_camera_offset_mean.linear();
            R.transposeInPlace();
            Matrix6<T> info;
            info.setZero();
            info.block(0,0,3,3)=R*_camera_offset_info.block(0,0,3,3)*R.transpose();
            info.block(3,3,3,3)=R*_camera_offset_info.block(3,3,3,3)*R.transpose();
            return info;
        }

        SensorParams3<T>* _camera_offset; /** \var pointer to a SensorParams3 set */
        Isometry3<T> _camera_offset_mean; /** \var the current mean of the camera */
        Isometry3<T> _inverse_camera_offset_mean; /** \var inverse of _camera_offset_mean */
        Matrix6<T> _camera_offset_info; /** \var camera offset info */

        /**
         * @brief prior error
         * @param delta_x, Vector6
         * @return VEctor6, the error
         */
        inline Vector6<T> error(const Vector6<T>& delta_x=Vector6<T>::Zero()) const {
            Isometry3<T> delta_X=v2t(delta_x);
            return t2v(_inverse_camera_offset_mean * (_camera_offset->isometry()) * delta_X);
        }

        /**
         * @brief prior jacobian
         * @return Matrix6, the jacobian
         */

        inline Matrix6<T> jacobian()  const {
            float epsilon = 1e-2;
            Matrix6<T> J;
            J.setZero();
            float two_inverse_epsilon = .5/epsilon;
            for (int i = 0; i<6; i++){
                    Vector6<T> increment=Vector6<T>::Zero();
                    increment(i) = epsilon;
                    J.col(i)=two_inverse_epsilon * (error(increment) - error(-increment));
                }
            return J;
        }

    };

    /** \typedef SolverPriorVector<T> is the template version of a std::vector of SolverPrior<T> */
    template <typename T>
    using SolverPriorVector = std::vector<SolverPrior<T>, Eigen::aligned_allocator<SolverPrior<T> > >;

    /** \typedef MultiSolverPrior<T> is the template version of a std::vector of SolverPriorVector<T> */
    template <typename T>
    using MultiSolverPrior = std::vector<SolverPriorVector<T>, Eigen::aligned_allocator<SolverPriorVector<T> > >;


}

#endif // PRIOR_HPP
