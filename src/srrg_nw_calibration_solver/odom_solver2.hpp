/**
 * nw_calibration - Heterogeneous Sensor Calibration Tool
 *
 *  Copyright (c) 2017, Bartolomeo Della Corte <dellacorte@diag.uniroma1.it>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _ODOM_SOLVER2_HPP
#define _ODOM_SOLVER2_HPP

#include "odom_base_solver.hpp"

namespace new_world_calibration{

  /**
   * OdomSolver2 class.
   * solver implementation for 2d problems, stores, computes and returns all the
   * needed matrices
   */
  template<typename T>
  class OdomSolver2 : public OdomBaseSolver<T>{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    /**
     * @brief a constructor
     */
    OdomSolver2(){}

    /**
     * @brief a destructor
     * nothing to be destroyed here. MultiSolver owns the dataset and params pointers
     */
    ~OdomSolver2(){}

    /**
     * @brief setter of SensorParams2
     * @param sensor_params_, pointer to SensorParams2<T>
     */
    void setSensorParams2(SensorParams2<T>* sensor_params_){_sensor_params = sensor_params_; _sensor_params->printStatus();}

    /**
     * @brief setter of Dataset2
     * @param dataset_, pointer to Dataset2<T>
     */
    void setDataset2(Dataset2<T>* dataset_){_dataset = dataset_;}

    /**
     * @brief getter of _sensor_params
     * @return pointer to SensorParams2
     */
    SensorParams2<T>* sensor_params(){return _sensor_params;}

    /**
     * @brief this methods compute the matrices needed for LS computation
     * practically it computes the H and b matrices of a 2d sub-problem
     */
    void computeMatrices();

    /**
     * @brief compute the jacobian for LS
     * @param sample, a pointer to Sample2
     * @return a MatrixX, i.e. the Jacobian
     */
    MatrixX<T> computeJacobian(Sample2<T>* sample);

    /**
     * @brief compute the error for LS
     * @param sample, pointer to Sample2
     * @param increment, index of the parameters to be incremented, default -1
     * @param sign, sign of increment to apply, default 0
     * @return a MatrixX, i.e. the error
     */
    MatrixX<T> computeError(Sample2<T>* sample, const int& increment = -1, const int& sign = 0);

  private:
    SensorParams2<T>* _sensor_params; /** \var pointer to a SensorParams2 set. MultiSolver is the owner */
    Dataset2<T>* _dataset; /** \var pointer to a Dataset2. MultiSolver is the owner */
  };


  /** \typedef Solver2Vector<T> is the template version of a std::vector of pointer to Solver2<T> */
  template <typename T>
  using OdomSolver2Vector = std::vector<OdomSolver2<T>* >;

}

#include "odom_solver2.cpp"

#endif
