# SRRG New World Calibration 

This library is header-only.
In order to use it include in your source

    #include "srrg_nw_calibration_solver/multi_solver.hpp"

## Prerequisites

* eigen3

## Installation
Standard catkin package installation

    git clone https://gitlab.com/srrg-software/srrg_nw_calibration.git
    catkin_make --pkg srrg_nw_calibration

## Packages that use this library

* [srrg_nw_calibration_ros](https://gitlab.com/srrg-software/srrg_nw_calibration_ros.git)

## Authors

* Bartolomeo Della Corte
* Giorgio Grisetti
* Maurilio Di Cicco

## Related Publications

* M.Di Cicco and B.Della Corte and G.Grisetti. "[**Unsupervised calibration of wheeled mobile platforms**](http://ieeexplore.ieee.org/document/7487631/)", In Proc. of the IEEE International Conference on Robotics and Automation (ICRA), Stockholm, 2016.
